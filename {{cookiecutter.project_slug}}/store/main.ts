export const state = () => ({
  counter: 0
})

export const mutations = {
  increment(state: { counter: number }) {
    state.counter++
  }
}
